# Dobble-Words Generator

To run, make a list of Q words (where Q is of the form n^2 + n + 1, and n is
prime) and put them in a text file, one word per line. In the example, we will
call the file "input_list.txt".

You can then run the script with the file to read as the argument.

You will need to install the Python dependencies. We recommend `pipenv` for
this:

```
$ pipenv install
$ pipenv run python file.py input_list.txt
```

The results will be stored in output.txt (unless there was an error).
