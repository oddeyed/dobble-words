import pytest

import numpy as np

import cards

@pytest.mark.parametrize("candidate,status", [
    (2, True),
    (3, True),
    (4, False),
    (5, True),
    (6, False),
    (7, True),
    (275604541, True),
    (275604540, False),

])
def test_primetest(candidate, status):
    assert cards.is_prime(candidate) == status


def test_nonprime_order():
    with pytest.raises(ValueError):
        cards.membership(4)


@pytest.mark.parametrize("size", (2, 3, 5, 7, 11))
def test_constraint(size):
    cards.check_membership(cards.membership(size))


def test_invalid_empty():
    bad = np.array([[0, 0, 0], [0, 0, 0]])
    with pytest.raises(ValueError):
        cards.check_membership(bad)


def test_invalid_two():
    bad = np.array([[1, 1, 0], [1, 1, 0]])
    with pytest.raises(ValueError):
        cards.check_membership(bad)
