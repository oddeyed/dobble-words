"""Generate cards from wordlist in file."""

__author__ = "Ben Janoff <ben.janoff@gmail.com>"
__copyright__ = "Copyright (c) Ben Janoff 2018"


import argparse
import numpy as np
import pprint
import random
import sys
from typing import Tuple, Sequence

import cards


def load_wordlist(filename: str) -> Tuple[int, Sequence[str]]:
    with open(filename) as f:
        words = tuple(line.strip().lower() for line in f)
        n = len(words)

    # If discriminant is non-square, then p is not an integer (still may be
    # composite though).
    discriminant = 1 + 4 * (n - 1)
    if not np.sqrt(discriminant).is_integer():
        raise ValueError("Invalid number of words in file. Number must be of "
                         "the form p^2 + p + 1 where p is prime.")

    order = int(np.sqrt(n))
    return (order, words)

def is_square(n):
    return


def main() -> None:
    parser = argparse.ArgumentParser()
    parser.add_argument('file', help='The file to load')

    args = parser.parse_args()
    try:
        order, words = load_wordlist(args.file)
    except OSError as exc:
        print(f"ERROR: Could not load file: {exc.args[0]}")
        sys.exit(1)
    except ValueError as exc:
        print(f"ERROR: {exc.args[0]}")
        sys.exit(1)

    membership = cards.membership(order)
    card_set = cards.make_cards(words, membership)

    with open('output.txt', 'w') as f:
        random.shuffle(card_set)
        for card in card_set:
            for word in card:
                f.write(f'{word} ')
            f.write('\b\r\n')

if __name__ == "__main__":
    main()
