"""Generate sets of Dobble-style cards from wordlist

Based on StackOverflow answer by Sven Zwei.
https://stackoverflow.com/a/47130477
"""

__author__ = "Ben Janoff <ben.janoff@gmail.com>"
__copyright__ = "Copyright (c) Ben Janoff 2018"


import argparse
import math
import pprint
import random
import sys
from typing import Sequence

import numpy as np


def is_prime(candidate: int) -> bool:
    """Return whether or not a positive integer is prime."""
    if candidate <= 0:
        raise ValueError("Cannot test primality of negative integer.")

    if candidate == 1:
        return False

    upper = math.sqrt(float(candidate))
    for d in range(2, math.floor(upper) + 1):
        if candidate % d == 0:
            return False

    return True


def membership(order: int) -> np.array:
    """Return an array of symbol -> card membership.

    Each row represents a card. The symbol i is set to 1 if that symbol is on
    that card, and is set to zero otherwise.
    """
    if not is_prime(order):
        raise ValueError("Order must be prime")

    # A deck of order n contains n^2 + n + 1 cards
    dim = order * order + order + 1
    res = np.zeros((dim, dim), dtype=np.int8)
    cur = 0

    # These correspond to the "vertical" lines on Zven's graph,
    # with the "infinite slope" card
    for i in range(order):
        # Each of these forms one column in the order * order symbol table
        shown = [i * order + j for j in range(order)] + [order * order]
        np.put(res[cur, :], shown, 1)
        cur += 1

    # For each non-infinite slope...
    for i in range(order):
        # For each column-zero intersection...
        for j in range(order):
            shown = [k * order + (j + i * k) % order
                     for k in range(order)] + [order * order + 1 + i]
            np.put(res[cur, :], shown, 1)
            cur += 1

    # Add the all-slopes card
    shown = [order * order + i for i in range(order + 1)]
    np.put(res[cur, :], shown, 1)
    cur += 1

    # Check we did the whole lot:
    assert cur == dim

    return res


def check_membership(membership: np.array) -> None:
    """Raises exception if a membership array is invalid."""
    for idx, left in enumerate(membership):
        # We compare 0-1, 0-2, ..., 1-2, 1-3, ..., 2-3, etc.
        for jdx, right in enumerate(membership[idx+1:], start=idx+1):
            combined = left & right
            if len(combined[combined == 1]) != 1:
                raise ValueError(f"Cards {idx} and {jdx} fail constraint: "
                                 f"membership of {left} and {right}. "
                                 f"Logical AND of rows: {combined}")


def main() -> None:
    """Argument parsing and starting"""
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument('order')

    args = parser.parse_args()
    order = int(args.order)

    try:
        cards = make_random(order)
        pprint.pprint(cards)
    except ValueError as exc:
        print(f"ERROR: {exc.args[0]}")
        sys.exit(1)


def make_cards(words: Sequence[str], membership: np.array) -> Sequence[np.array]:
    npwords = np.array(words)
    return [npwords[members == 1] for members in membership]


def make_random(order: int) -> np.array:
    """Make a random selection of cards of order provided."""
    n = order * order + order + 1

    wordfile = './words'
    with open(wordfile) as f:
        nwords = sum(1 for _ in f)
        indices = random.sample(range(nwords), k=n)
    with open(wordfile) as f:
        words = [word.strip() for idx, word in enumerate(f) if idx in indices]

    return make_cards(words, membership(order))


if __name__ == "__main__":
    main()
